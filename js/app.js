
/***************** Acordeón para Betterware *****************/
/***************** Desarrollado por CMAGANA *****************/

/***************** CONSTRUCTOR *****************/


function Nivel(){
    this.nivel = {};
    this.base_url = "";
    this.api_paginado = "http://ec2-52-91-203-171.compute-1.amazonaws.com:8000/ws/get_monitoreo_drilldown/?clave=";
}


/***************** AJAX REQUESTs *****************/


// Cabecera
Nivel.prototype._getNivelHead = function(){
    var self = this;
	$('#modal-wait').modal('show');

	$.ajax({
		url 		: self.api_paginado + self.nivel.id,
		method 		: 'GET',
		dataType	: 'JSON',
		data 		: self.nivel
	})
	.done( function( _res ){
		$('#modal-wait').modal('hide');
		console.log( _res )
		self._drawNivelHead( _res );
	})
	.fail( function( _err ){
		$('#modal-wait').modal('hide');
		//alert("No existen datos para esta persona");
		console.log( _err );
	})
}

//Sub Nivel
Nivel.prototype._getNivelSub = function(){
    var self = this;
	$('#modal-wait').modal('show');

	$.ajax({
		url 		: self.api_paginado + self.nivel.key,
		method 		: 'GET',
		dataType	: 'JSON'
	})
	.done( function( _res ){
		$('#modal-wait').modal('hide');
		console.log( _res )
		self._drawNivelSub( _res );
	})
	.fail( function( _err ){
		$('#modal-wait').modal('hide');
		// Se devuelve el ícono a verde
		$('#' + self.nivel.key).prev().removeClass('shown').find("td:first-child").removeClass("details-control");
		//alert("No existen datos para esta persona");
		console.log( _err );
	})
}

// Páginado
Nivel.prototype._getNivelSubMore = function(){
    var self = this;
	$('#modal-wait').modal('show');

	$.ajax({
		url 		: self.api_paginado + self.nivel.key + "&page=" + self.nivel.page,
		method 		: 'GET',
		dataType	: 'JSON'
	})
	.done( function( _res ){
		$('#modal-wait').modal('hide');
		console.log( _res )
		self._drawNivelSubMore( _res );
	})
	.fail( function( _err ){
		$('#modal-wait').modal('hide');
		//alert("No existen datos para esta persona");
		console.log( _err );
	})
}


/***************** Seteo  *****************/


Nivel.prototype._set = function( _data ){
    this.nivel.key    = _data._key || null;
    this.nivel.id     = _data._id || null;
    this.nivel.level  = _data._level || null;
    this.nivel.page   = _data._page || null;
    this.nivel.method = _data._method || null;

    if (this.nivel.method === "getNivelHead"){ 
        this._getNivelHead();
    }
    if (this.nivel.method === "getNivelSub"){ 
        this._getNivelSub();
    }
    if (this.nivel.method === "getNivelSubMore"){ 
        this._getNivelSubMore();
    }
}


/***************** Funciones comúnes **************/


// Formatea tipo moneda
Number.prototype._formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return "$ " + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

// Regresa datos formateados
Nivel.prototype._formatData = function( _data ){
	
	var odd;

	// Header
	for (var i = 0; i <= _data.header.length - 1; i++) {
		
		// Rojo si es menor de 90%, amarillo si es menor de %100 y verde si pasa del 100%
		if ( _data.header[i].PctCumplido <= 90 ){
			_data.header[i].prctColor = "colorRojo";
		} else if ( _data.header[i].PctCumplido > 90  && _data.header[i].PctCumplido <= 100 ){
			_data.header[i].prctColor = "colorAmarillo";
		} else if ( _data.header[i].PctCumplido > 100 ){
			_data.header[i].prctColor = "colorVerde";
		}
		// Sin decimales
		var cantidad = +_data.header[i].PorcActAsociadas;
		_data.header[i].PorcActAsociadas = cantidad.toFixed();
		// Con un decimal
		var cantidad = +_data.header[i].PctCumplido;
		_data.header[i].PctCumplido = cantidad.toFixed(1);
		// Con un decimal
		var cantidad = +_data.header[i].PctActividad;
		_data.header[i].PctActividad = cantidad.toFixed(1);
		// Con dos decimales
		var cantidad = +_data.header[i].Venta;
		_data.header[i].Venta = (cantidad)._formatMoney(2, '.', ',');
		// Con dos decimales
		var cantidad = +_data.header[i].Objetivos;
		_data.header[i].Objetivos = (cantidad)._formatMoney(2, '.', ',');

	};

	// Detalle
	for (var i = 0; i <= _data.detalles.length - 1; i++) {
		
		// Rojo si es menor de 90%, amarillo si es menor de %100 y verde si pasa del 100%
		if ( _data.detalles[i].PctCumplido <= 90 ){
			_data.detalles[i].prctColor = "colorRojo";
		} else if ( _data.detalles[i].PctCumplido > 90  && _data.detalles[i].PctCumplido <= 100 ){
			_data.detalles[i].prctColor = "colorAmarillo";
		} else if ( _data.detalles[i].PctCumplido > 100 ){
			_data.detalles[i].prctColor = "colorVerde";
		}
		// Sin decimales
		var cantidad = +_data.detalles[i].PorcActAsociadas;
		_data.detalles[i].PorcActAsociadas = cantidad.toFixed();
		// Con un decimal
		var cantidad = +_data.detalles[i].PctCumplido;
		_data.detalles[i].PctCumplido = cantidad.toFixed(1);
		// Con un decimal
		var cantidad = +_data.detalles[i].PctActividad;
		_data.detalles[i].PctActividad = cantidad.toFixed(1);
		// Con dos decimales
		var cantidad = +_data.detalles[i].Venta;
		_data.detalles[i].Venta = (cantidad)._formatMoney(2, '.', ',');
		// Con dos decimales
		var cantidad = +_data.detalles[i].Objetivos;
		_data.detalles[i].Objetivos = (cantidad)._formatMoney(2, '.', ',');
		// Rows estilo Zebra
		odd = !(odd);
		_data.detalles[i].odd = odd;

	};

	return _data;
}


/***************** Handlebars  *****************/


// Cabecera
Nivel.prototype._drawNivelHead = function( _data ){

	var self = this;
	var header = _data.header;

	if ( header !== undefined && header.length > 0 ){

		$.ajax({
			url: "templates/header/nivel" + _data.header[0].nivel + "cabecera.hbs",
			dataType: "html",
			success: function (_template) {

				// Formateo de datos	
				_data = self._formatData(_data);

				// COMPILAR TEMPLATE
				var template = Handlebars.compile( _template );
				var html = template( _data );
				$("#nivelInitial").addClass('animated fadeIn').html( html );
				
				if(_data.page >= _data.total_pages){
					$("#more" + self.nivel.id ).next().remove();
				} 
				activarFunciones();

			}
		});

	} else {
		$("#nivelInitial").html('<p class="text-center">No existen datos para esta persona</p>');
	}
};

// Sub Nivel
Nivel.prototype._drawNivelSub = function( _data ){
	var self = this;
	var detalles = _data.detalles;

	if (self.nivel.level === null) self.nivel.level = 0;

	if ( detalles !== undefined && detalles.length > 0 ){

		$.ajax({
			url: "templates/nivel/nivel" + _data.header[0].nivel_hijos + ".hbs",
			dataType: "html",
			success: function (_template) {

				// Formateo de datos	
				_data = self._formatData(_data);

				// COMPILAR TEMPLATE
				var template = Handlebars.compile( _template );
				var html = template( _data );
				$("#" + self.nivel.key).find('td').addClass('animated fadeIn').html( html );

				if(_data.page >= _data.total_pages){
					$("#more" + self.nivel.key).next().remove();
				} 
				activarFunciones();

			}
		});

	} else {
		$("#" + self.nivel.key).find('td').html('<p class="text-center">No existen datos para esta persona</p>');
	}
};

// Añadir más, Páginado
Nivel.prototype._drawNivelSubMore = function( _data ){
	var self = this;
	var detalles = _data.detalles;

	if (self.nivel.level === null) self.nivel.level = 0;

	if ( detalles !== undefined && detalles.length > 0 ){

		$.ajax({
			url: "templates/more/nivel" + _data.header[0].nivel_hijos + "more.hbs",
			dataType: "html",
			success: function (_template) {

				// Formateo de datos	
				_data = self._formatData(_data);

				// COMPILAR TEMPLATE
				var template = Handlebars.compile( _template );
				var html = template( _data );
				$("#more" + self.nivel.key).next().find('td').data( 'page', _data.page );
				$("#more" + self.nivel.key).addClass('animated fadeIn').append( html );

				if(_data.page >= _data.total_pages){
					$("#more" + self.nivel.key).next().remove();
				} 
				activarFunciones();

			}
		});

	} else {
		$("#more" + self.nivel.key).next().find('td').text('<p class="text-center">No existen más datos para esta persona</p>');
	}
};


/***************** Acciones  *****************/


function activarFunciones(){

	// Click, despliega hijos
	//$("tbody.data td.details-control").off("click").on("click", function(e){
	$("tbody.data>tr").off("click").on("click", function(e){
		e.preventDefault();
		e.stopPropagation();

        //var tr = $(this).closest('tr');
        var tr = $(this);
		var key = tr.data("key");
		var nivelhijos = tr.data("nivelhijos");
		var datanivel = tr.data("nivel");

		var nivel = new Nivel();
		var params = {
			_key    : key,
			_level  : nivelhijos,
			_method : 'getNivelSub'
		}

		// Efecto acordeon
		if (tr.hasClass("shown")) {
			
			// Cerrar todo los hijos abiertos
			var trshown = tr.next().find("tr.shown");
			$.each(trshown, function(){
				$(this).removeClass('shown').next().find("td:first-child").slideToggle();
			});

			// Cerrar actual
			tr.removeClass('shown').next().find('td:first-child').slideToggle();
		} else {
			
			// Cerrar todo los hermanos abiertos
			var trshown = tr.siblings("tr.shown");
			$.each(trshown, function(){
				$(this).removeClass('shown').next().find("td:first-child").slideToggle();
			});
			
			// Abrir
			if (tr.hasClass("showed")) {
				// Reabrir
				tr.addClass('shown');
				tr.next().find('td:first-child').slideToggle();
			} else {
				// Por primera vez
				nivel._set(params);
				tr.addClass('shown showed');
			}
		}
	});

	// Añadir más registros
	$("td.btnMas").off("click").on("click", function(e){
		e.preventDefault();

		var key = $(this).data("key");
		var page = $(this).data("page");
		var nivelhijos = $(this).data("nivelhijos");

		var nivel = new Nivel();
		var params = {
			_key    : key,
			_page   : page + 1,
			_level  : nivelhijos,
			_method : 'getNivelSubMore'
		}
		nivel._set(params);
	});

}

// Pasa al siguiente nivel parametros por POST
function openNextLevel(key){
	$("#keyParam").val(key);
	$("#formTemplate").submit();
}