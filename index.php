<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Monitoreo Semanal</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="css/style.css">
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- Leave those next 4 lines if you care about users using IE8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <div id="content">
        <div class="text-center monitoreo"><img src="assets/icono_monitoreo.png"> Monitoreo semanal</div><hr>
        <div class="text-center semana"><img src="assets/icono_semana.png"> Semana <span id="num-semana"></span></div>
        <!-- Carga de datos nivel cabecera -->  
        <div id="nivelInitial"></div>
    </div>

    <!-- Modal Loading -->  
    <div class="modal fade" id="modal-wait" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <h1 class="text-center"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...</h1>
            </div>
        </div>
    </div>

    <!-- Formulario para el paso de llave por POST -->   
    <form action="index.php" method="post" id="formTemplate">
        <input type="hidden" name="key" id="keyParam" />
    </form>

    <!-- Including Bootstrap JS (with its jQuery dependency) so that dynamic components work -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="handlebars/handlebars.js"></script>
    <script src="js/app.js"></script>

    <script>
        <?php 
            // Obtener key
            if(isset($_POST["key"])) {
                $key = $_POST["key"];
            } else if(isset($_GET["key"])){
                $key = base64_decode($_GET["key"]);
            } else {
                $key = "";
            }
        ?>
        $(function(){
            // Obtiene y renderiza datos a partir de una clave de usuario
            var nivel = new Nivel();
            var params = {
                _id     : '<?php echo $key ?>', 
                _method : 'getNivelHead'
            }
            nivel._set(params);
        });
    </script>

  </body>
</html>